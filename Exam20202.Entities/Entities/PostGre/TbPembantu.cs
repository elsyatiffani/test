﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exam20202.Entities.Entities.PostGre
{
    public partial class TbPembantu
    {
        public TbPembantu()
        {
            TbPencucian = new HashSet<TbPencucian>();
        }

        [Key]
        public int IdPembantu { get; set; }
        [Required]
        [StringLength(50)]
        public string NamaPembantu { get; set; }
        [Required]
        [StringLength(50)]
        public string AlamatPembantu { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime CreatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime UpdatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string UpdatedBy { get; set; }

        [InverseProperty("IdPembantuNavigation")]
        public virtual ICollection<TbPencucian> TbPencucian { get; set; }
    }
}
