﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exam20202.Entities.Entities.PostGre
{
    public partial class TbPiring
    {
        public TbPiring()
        {
            TbPencucian = new HashSet<TbPencucian>();
        }

        [Key]
        public int IdPiring { get; set; }
        [Required]
        [StringLength(50)]
        public string JenisPiring { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal HargaPiring { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime CreatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime UpdatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string UpdatedBy { get; set; }

        [InverseProperty("IdPiringNavigation")]
        public virtual ICollection<TbPencucian> TbPencucian { get; set; }
    }
}
