﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exam20202.Entities.Entities.PostGre
{
    public partial class TbPencucian
    {
        [Key]
        public int IdPencucian { get; set; }
        [Required]
        [StringLength(50)]
        public string TipePencucian { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTimeOffset TanggalPencucian { get; set; }
        public int IdPembantu { get; set; }
        public int IdPiring { get; set; }
        public int IsSelesai { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? TotalPencucian { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime CreatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime UpdatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string UpdatedBy { get; set; }

        [ForeignKey(nameof(IdPembantu))]
        [InverseProperty(nameof(TbPembantu.TbPencucian))]
        public virtual TbPembantu IdPembantuNavigation { get; set; }
        [ForeignKey(nameof(IdPiring))]
        [InverseProperty(nameof(TbPiring.TbPencucian))]
        public virtual TbPiring IdPiringNavigation { get; set; }
    }
}
