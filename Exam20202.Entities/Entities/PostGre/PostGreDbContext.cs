﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Exam20202.Entities.Entities.PostGre
{
    public partial class PostGreDbContext : DbContext
    {
      
        public PostGreDbContext(DbContextOptions<PostGreDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbPembantu> TbPembantu { get; set; }
        public virtual DbSet<TbPencucian> TbPencucian { get; set; }
        public virtual DbSet<TbPiring> TbPiring { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseNpgsql("Server=localhost;Database=Db_Quiz2;Username=postgres;Password=postgres;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbPembantu>(entity =>
            {
                entity.Property(e => e.IdPembantu).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.CreatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.UpdatedBy).HasDefaultValueSql("'SYSTEM'::character varying");
            });

            modelBuilder.Entity<TbPencucian>(entity =>
            {
                entity.Property(e => e.IdPencucian).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.CreatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.UpdatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.HasOne(d => d.IdPembantuNavigation)
                    .WithMany(p => p.TbPencucian)
                    .HasForeignKey(d => d.IdPembantu)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TbPencucian_TbPembantu");

                entity.HasOne(d => d.IdPiringNavigation)
                    .WithMany(p => p.TbPencucian)
                    .HasForeignKey(d => d.IdPiring)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TbPencucian_TbPiring");
            });

            modelBuilder.Entity<TbPiring>(entity =>
            {
                entity.Property(e => e.IdPiring).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.CreatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.UpdatedBy).HasDefaultValueSql("'SYSTEM'::character varying");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
