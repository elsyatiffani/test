INSERT INTO "TbPiring"("JenisPiring", "HargaPiring", "CreatedAt", "CreatedBy")
VALUES ('Keramik', 20000, now(), 'Asep');
INSERT INTO "TbPiring"("JenisPiring", "HargaPiring", "CreatedAt", "CreatedBy")
VALUES ('Plastik', 10000, now(), 'Asep');

INSERT INTO "TbPembantu"("NamaPembantu", "AlamatPembantu", "CreatedAt", "CreatedBy")
VALUES ('Ainun', 'Semarang', '2020-03-25', 'Bambang');
INSERT INTO "TbPembantu"("NamaPembantu", "AlamatPembantu", "CreatedAt", "CreatedBy")
VALUES ('Suminah', 'Surabaya', '2020-03-25', 'Bambang');

INSERT INTO "TbPencucian"("TipePencucian", "TanggalPencucian", "IdPembantu", "IdPiring", "IsSelesai", "TotalPencucian")
VALUES ('Manual', '2020-03-25',1, 2, '1', 30);
INSERT INTO "TbPencucian"("TipePencucian", "TanggalPencucian", "IdPembantu", "IdPiring", "IsSelesai", "TotalPencucian")
VALUES ('Dish Washer', '2020-03-25',2, 1, '1', 40);