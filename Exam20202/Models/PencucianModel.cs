﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam20202.Models
{
    public class PencucianModel
    {
        /// <summary>
        /// Id pencucian dari database "TbPencucian"
        /// </summary>
        public int IdPencucian { set; get; }

        /// <summary>
        /// Id Pembantu dari database "TbPembantu"
        /// </summary>
        public int IdPembantu { set; get; }

        /// <summary>
        /// Id Piring dari database "TbPiring"
        /// </summary>
        public int IdPiring { set; get; }

        /// <summary>
        /// Jenis Piring (Nama Piring) dari database "TbPiring"
        /// </summary>
        public string JenisPiring { set; get; }

        /// <summary>
        /// Nama Pembantu dari database "TbPembantu"
        /// </summary>
        public string NamaPembantu { set; get; }

        /// <summary>
        /// Total Pencucian dari database "TbPencucian"
        /// </summary>
        public decimal? TotalPencucian { set; get; }

        /// <summary>
        /// Tanggal pencucian dari database "TbPencucian"
        /// </summary>
        public DateTimeOffset TanggalPencucian { set; get; }

        /// <summary>
        /// Tipe Pencucian (Nama Pencucian) dari database "TbPencucian"
        /// </summary>
        public string TipePencucian { set; get; }
       
    }
}
