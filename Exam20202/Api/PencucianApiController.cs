﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam20202.Models;
using Exam20202.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam20202.Api
{
    [Route("api/v1/pencucian-app")]
    [ApiController]
    public class PencucianApiController : ControllerBase
    {
        private readonly PencucianService _pencucianAppMan;
        public PencucianApiController(PencucianService pencucianService)
        {
            this._pencucianAppMan = pencucianService;
        }

        [HttpGet("get-all", Name = "getAll")]
        public async Task<ActionResult<List<PencucianModel>>> GetAllApi()
        {
            var data = await _pencucianAppMan.GetAllData();
            if (data == null)
            {
                return null;
            }

            return Ok(data);
        }

        [HttpPost("insert-Pencucian", Name = "insertPencucian")]
        public async Task<ActionResult<ResponseModel>> InsertTransactionAsync([FromBody]PencucianModel value)
        {
            var isSuccess = await this._pencucianAppMan.InsertPencucian(value);
            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Failed to insert"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = "Success to insert Pencucian"
            });
        }

        [HttpDelete("delete-pencucian", Name = "deletePencucian")]
        public async Task<ActionResult<ResponseModel>> DeleteTransactionAsync([FromBody] PencucianModel pencucianModel)
        {
            var isSuccess = await this._pencucianAppMan.DeletePencucianAsync(pencucianModel.IdPencucian);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Failed to delete"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success to delete transaction {pencucianModel.IdPencucian}"
            });
        }


        [HttpGet("get-specific-pencucian", Name = "getSpecificPencucian")]
        public async Task<ActionResult<PencucianModel>> GetSpecificPencucianAsync(int? IdPencucian)
        {
            if (IdPencucian.HasValue == false)
            {
                return BadRequest(null);
            }

            var pencucian = await this._pencucianAppMan.GetSpecificPencucian(IdPencucian.Value);

            if (pencucian == null)
            {
                return BadRequest(null);
            }
            return Ok(pencucian);
        }


        [HttpPut("update-pencucian", Name = "updatePencucian")]
        public async Task<ActionResult<ResponseModel>> UpdatePencucianAsync([FromBody]PencucianModel pencucianModel)
        {
            var isSuccess = await this._pencucianAppMan.UpdatePencucianAsync(pencucianModel);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Pencucian ID not found!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Update Pencucian {pencucianModel.IdPencucian} successfully!"
            });
        }

        [HttpGet("filter-data")]
        public async Task<ActionResult<List<PencucianModel>>> GetFilterDataAsync([FromQuery] int pageIndex, int itemPerPage, string filterByName)
        {
            var data = await _pencucianAppMan.GetAsync(pageIndex, itemPerPage, filterByName);

            return Ok(data);
        }
    }
}
