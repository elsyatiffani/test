﻿using Exam20202.Entities.Entities.PostGre;
using Exam20202.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam20202.Services
{
    public class PencucianService
    {
        private readonly PostGreDbContext _pencucianDbContext;
        public PencucianService(PostGreDbContext dbContext)
        {
            this._pencucianDbContext = dbContext;
        }

        /// <summary>
        /// Untuk Menampilkan semua data
        /// </summary>
        /// <returns></returns>
        public async Task<List<PencucianModel>> GetAllData()
        {
            var query = from pnc in _pencucianDbContext.TbPencucian
                        join prg in _pencucianDbContext.TbPiring
                        on pnc.IdPiring equals prg.IdPiring
                        join pmb in _pencucianDbContext.TbPembantu
                        on pnc.IdPembantu equals pmb.IdPembantu
                        select new PencucianModel
                        {
                            IdPencucian = pnc.IdPencucian,
                            JenisPiring = prg.JenisPiring,
                            NamaPembantu = pmb.NamaPembantu,
                            TanggalPencucian = pnc.TanggalPencucian,
                            TotalPencucian = pnc.TotalPencucian
                        };
            var data = await query.AsNoTracking().ToListAsync();
           
            return data;
        }

        /// <summary>
        /// Untuk input data
        /// </summary>
        /// <param name="pencucianModel"></param>
        /// <returns></returns>
        public async Task<bool> InsertPencucian(PencucianModel pencucianModel)
        {
           
            this._pencucianDbContext.Add(new TbPencucian
            {
                TipePencucian = pencucianModel.TipePencucian,
                TanggalPencucian = pencucianModel.TanggalPencucian,
                IdPembantu = pencucianModel.IdPembantu,
                IdPiring = pencucianModel.IdPiring
            });;

            await this._pencucianDbContext.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Untuk update data
        /// </summary>
        /// <param name="pencucianModel"></param>
        /// <returns></returns>
        public async Task<bool> UpdatePencucianAsync(PencucianModel pencucianModel)
        {
            var pencucianmodel = await this._pencucianDbContext.TbPencucian.Where(Q => Q.IdPencucian == pencucianModel.IdPencucian).FirstOrDefaultAsync();

            if (pencucianmodel == null)
            {
                return false;
            }

            pencucianmodel.IdPiring = pencucianModel.IdPiring;
            pencucianmodel.IdPembantu = pencucianModel.IdPembantu;
            pencucianmodel.TipePencucian = pencucianModel.TipePencucian;
            pencucianmodel.TanggalPencucian = pencucianModel.TanggalPencucian;

            await this._pencucianDbContext.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Untuk hapus data
        /// </summary>
        /// <param name="IdPencucian"></param>
        /// <returns></returns>
        public async Task<bool> DeletePencucianAsync(int IdPencucian)
        {
            var pencucianModel = await this._pencucianDbContext.TbPencucian.Where(Q => Q.IdPencucian == IdPencucian).FirstOrDefaultAsync();

            if (pencucianModel == null)
            {
                return false;
            }

            this._pencucianDbContext.Remove(pencucianModel);
            await this._pencucianDbContext.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Untuk mendapatkan data yang lebih spesifik agar dapat melakukan update
        /// </summary>
        /// <param name="IdPencucian"></param>
        /// <returns></returns>
        public async Task<PencucianModel> GetSpecificPencucian(int? IdPencucian)
        {
            var pencucian = await this._pencucianDbContext.TbPencucian.Where(Q => Q.IdPencucian == IdPencucian).
                Select(Q => new PencucianModel {
                IdPencucian = Q.IdPencucian
            }).FirstOrDefaultAsync();

            return pencucian;
        }

        /// <summary>
        /// Untuk filter data
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="itemPerPage"></param>
        /// <param name="filterByName"></param>
        /// <returns></returns>

        public async Task<List<PencucianModel>> GetAsync(int pageIndex, int itemPerPage, string filterByName)
        {
            // set employee to queryable for  filter
            var query = this._pencucianDbContext.TbPembantu.AsQueryable();
               
            // for filter
            if (string.IsNullOrEmpty(filterByName) == false)
            {
                query = query
                    .Where(Q => Q.NamaPembantu.StartsWith(filterByName));
            }

            // get data after filtering
            var pencucian = await query
                .Select(Q => new PencucianModel
                {
                    IdPembantu = Q.IdPembantu
                })
                .Skip((pageIndex - 1) * itemPerPage)
                .Take(itemPerPage)
                .AsNoTracking()
                .ToListAsync();

            return pencucian;
        }

    }
}
